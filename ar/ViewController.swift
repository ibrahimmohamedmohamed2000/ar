//
//  ViewController.swift
//  ar
//
//  Created by IFTS12 on 27/02/22.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        let scene = SCNScene(named: "art.scnassets/RubikCube copy.scn")
        print()
        if let node = scene?.rootNode.childNodes.first{
            node.position = SCNVector3(0, 0, -0.5)
            
        }
        sceneView.autoenablesDefaultLighting = true
        sceneView.scene = scene!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARPositionalTrackingConfiguration()
        
        sceneView.debugOptions = ARSCNDebugOptions.showFeaturePoints

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    

    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }

    
}
